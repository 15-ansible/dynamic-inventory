### Demo Project:
Configure Dynamic Inventory
### Technologies used:
Ansible, Terraform, AWS
### Project Description:
- Provision EC2 Instance with Terraform
- Configure Ansible AWS EC2 Plugin to dynamically sets
inventory of EC2 servers that Ansible should manage,
instead of hard-coding server addresses in Ansible
inventory file